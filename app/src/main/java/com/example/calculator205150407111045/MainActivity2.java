package com.example.calculator205150407111045;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;

public class MainActivity2 extends AppCompatActivity {
    private TextView hasil;
    private TextView solusi;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        hasil = findViewById(R.id.hasil);
        solusi = findViewById(R.id.solusi);

        String finalResult = getIntent().getStringExtra("finalResult");
        String solution =  getIntent().getStringExtra("solution");

        hasil.setText(finalResult);
        solusi.setText(solution);
    }
}